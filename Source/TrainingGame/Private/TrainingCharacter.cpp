// Fill out your copyright notice in the Description page of Project Settings.


#include "TrainingCharacter.h"

// Sets default values
ATrainingCharacter::ATrainingCharacter(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer.SetDefaultSubobjectClass<UTrCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComp");
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UTrainingCharacterAttributeSet::GetManaAttribute()).AddUObject(this, &ATrainingCharacter::OnManaUpdated);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UTrainingCharacterAttributeSet::GetHealthAttribute()).AddUObject(this, &ATrainingCharacter::OnHealthUpdated);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(UTrainingCharacterAttributeSet::GetStaminaAttribute()).AddUObject(this, &ATrainingCharacter::OnStaminaUpdated);
	
	AttributeSet = CreateDefaultSubobject<UTrainingCharacterAttributeSet>("Attributes");

//	GiveAbilities();
}

// Called when the game starts or when spawned
void ATrainingCharacter::BeginPlay()
{
	Super::BeginPlay();

//	AbilitySystemComponent->InitAbilityActorInfo(this, this);
//	InitializeAttributes();
//	GiveAbilities();
		

	//AbilitySystemComponent = new UAbilitySystemComponent();
	
	
}

// Called every frame
void ATrainingCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void ATrainingCharacter::InitializeAttributes()
{
	if (AbilitySystemComponent && DefaultAttributesEffect) {
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);
		FGameplayEffectSpecHandle SpecHandle = AbilitySystemComponent->MakeOutgoingSpec(DefaultAttributesEffect, 1, EffectContext);

		if (SpecHandle.IsValid()) {
			FActiveGameplayEffectHandle GEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data.Get());
		}
	}

	AttributeSet->InitStamina(MaxStamina);
	AttributeSet->InitHealth(MaxHealth);
	AttributeSet->InitMana(MaxMana);
}

void ATrainingCharacter::GiveAbilities()
{
	if (AbilitySystemComponent) {
		for (TSubclassOf<UGameplayAbility>& StartupAbility : DefaultAbilities)
		{
			AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility.GetDefaultObject(), 1, 0));
		}
	}
}

UAbilitySystemComponent* ATrainingCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ATrainingCharacter::OnManaUpdated(const FOnAttributeChangeData& Data)
{
	OnManaChange.Broadcast(Data.NewValue);
}

void ATrainingCharacter::OnHealthUpdated(const FOnAttributeChangeData& Data)
{
	OnHealthChange.Broadcast(Data.NewValue);
}

void ATrainingCharacter::OnStaminaUpdated(const FOnAttributeChangeData& Data)
{
	OnStaminaChange.Broadcast(Data.NewValue);
}

