// Fill out your copyright notice in the Description page of Project Settings.

#include "TrCharacterMovementComponent.h"
#include "AbilitySystemComponent.h"
#include "GameFramework/Actor.h"
#include "TrainingCharacter.h"
#include "TrainingCharacterAttributeSet.h"

void UTrCharacterMovementComponent::BeginPlay()
{
    Super::BeginPlay();

    ATrainingCharacter* MyCharacterOwner = Cast<ATrainingCharacter>(GetOwner());
    if (MyCharacterOwner)
    {
//        UAbilitySystemComponent* AbilitySystemComp = MyPawnOwner->FindComponentByClass<UAbilitySystemComponent>();
//        if (AbilitySystemComp)
//        {
            // Assuming your AttributeSet is already initialized at this point
        AttributeSetRef = MyCharacterOwner->AttributeSet;
//        }
    }
}

float UTrCharacterMovementComponent::GetMaxSpeed() const
{
    if (AttributeSetRef)
    {
        float MovementSpeed = AttributeSetRef->GetMovementSpeed();

      /*  if (GEngine)
        {
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("My Float Value: %f"), MovementSpeed));
        }*/
        return MovementSpeed;

    }
    else {
        if (GEngine)
        {
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("fail"));
        }
    }

    // Fallback to default behavior if we can't get the custom speed
    return Super::GetMaxSpeed();

}
