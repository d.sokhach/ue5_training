// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TrainingCharacter.h"
#include "TrainingPlayerCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TRAININGGAME_API ATrainingPlayerCharacter : public ATrainingCharacter
{
	GENERATED_BODY()

public:

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PossessedBy(AController* NewController) override;
	virtual void OnRep_PlayerState() override;
	
};
