// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "GMC_ClampedRegen.generated.h"

/**
 * 
 */
UCLASS()
class TRAININGGAME_API UGMC_ClampedRegen : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
private:

	UGMC_ClampedRegen();

public:

	float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const;

};
