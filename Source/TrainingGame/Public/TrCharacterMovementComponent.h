// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TrainingCharacterAttributeSet.h"
#include "TrCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class TRAININGGAME_API UTrCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

public:
	virtual float GetMaxSpeed() const override;

private:
	// Reference to the AttributeSet for quick access
	const UTrainingCharacterAttributeSet* AttributeSetRef = nullptr;

};
