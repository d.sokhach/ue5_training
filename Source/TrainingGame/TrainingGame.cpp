// Copyright Epic Games, Inc. All Rights Reserved.

#include "TrainingGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TrainingGame, "TrainingGame" );
